var expect = require('expect');
var helpers = require('../src/helpers');

describe('test of eachLoop', function() {
    it('should not call callback function', function() {
        var object = {
            eachLoopCallBack : function() {}
        };
        var spy = expect.spyOn(object, 'eachLoopCallBack');

        helpers.eachLoop(undefined, object.eachLoopCallBack);
        expect(spy).toNotHaveBeenCalled();

        helpers.eachLoop(null, object.eachLoopCallBack);
        expect(spy).toNotHaveBeenCalled();
    });

    it('should skip b and c (forward)', function() {
        var expectedReturn = ['a', 'd'];
        var eachLoopReturn = [];
        var object = {
            eachLoopCallBack : function(element, index) {
                if(element === 'b' || element === 'c'){
                    return true;
                }
                eachLoopReturn.push(element)
            }
        };
        var spy = expect.spyOn(object, 'eachLoopCallBack').andCallThrough();
        var testArray = ['a', 'b', 'c', 'd'];

        helpers.eachLoop(testArray, object.eachLoopCallBack);
        expect(spy.calls.length).toEqual(4);
        expect(eachLoopReturn).toEqual(expectedReturn);
    });

    it('should break after b (forward)', function() {
        var expectedReturn = ['a', 'b'];
        var eachLoopReturn = [];
        var object = {
            eachLoopCallBack : function(element, index) {
                if(element === 'c'){
                    return false;
                }
                eachLoopReturn.push(element)
            }
        };
        var spy = expect.spyOn(object, 'eachLoopCallBack').andCallThrough();
        var testArray = ['a', 'b', 'c', 'd'];

        helpers.eachLoop(testArray, object.eachLoopCallBack);
        expect(spy.calls.length).toEqual(3);
        expect(eachLoopReturn).toEqual(expectedReturn);
    });

    it('should loop through whole array (forward)', function() {
        var eachLoopReturn = [];
        var object = {
            eachLoopCallBack : function(element, index) {
                eachLoopReturn.push(element)
            }
        };
        var spy = expect.spyOn(object, 'eachLoopCallBack').andCallThrough();
        var testArray = ['a', 'b', 'c', 'd'];

        helpers.eachLoop(testArray, object.eachLoopCallBack);
        expect(spy.calls.length).toEqual(4);
        expect(eachLoopReturn).toEqual(testArray);

    });

    it('should skip b and c (backwards)', function() {
        var expectedReturn = ['d', 'a'];
        var eachLoopReturn = [];
        var object = {
            eachLoopCallBack : function(element, index) {
                if(element === 'b' || element === 'c'){
                    return true;
                }
                eachLoopReturn.push(element)
            }
        };
        var spy = expect.spyOn(object, 'eachLoopCallBack').andCallThrough();
        var testArray = ['a', 'b', 'c', 'd'];

        helpers.eachLoop(testArray, object.eachLoopCallBack, true);
        expect(spy.calls.length).toEqual(4);
        expect(eachLoopReturn).toEqual(expectedReturn);
    });

    it('should break after b (backwards)', function() {
        var expectedReturn = ['d', 'c'];
        var eachLoopReturn = [];
        var object = {
            eachLoopCallBack : function(element, index) {
                if(element === 'b'){
                    return false;
                }
                eachLoopReturn.push(element)
            }
        };
        var spy = expect.spyOn(object, 'eachLoopCallBack').andCallThrough();
        var testArray = ['a', 'b', 'c', 'd'];

        helpers.eachLoop(testArray, object.eachLoopCallBack, true);
        expect(spy.calls.length).toEqual(3);
        expect(eachLoopReturn).toEqual(expectedReturn);
    });

    it('should loop through whole array (backwards)', function() {
        var eachLoopReturn = [];
        var expectedReturn = ['d', 'c', 'b', 'a'];
        var object = {
            eachLoopCallBack : function(element, index) {
                eachLoopReturn.push(element)
            }
        };
        var spy = expect.spyOn(object, 'eachLoopCallBack').andCallThrough();
        var testArray = ['a', 'b', 'c', 'd'];

        helpers.eachLoop(testArray, object.eachLoopCallBack, true);
        expect(spy.calls.length).toEqual(4);
        expect(eachLoopReturn).toEqual(expectedReturn);

    });
});