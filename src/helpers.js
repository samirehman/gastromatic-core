
/**
 * @desc    use native loop with lodash syntax
 * @param {Array}       array
 * @param {Function}    iteratee; return true for continue or return false for break
 * @param {Boolean}     reverse; loop array backwards if true
 */
function eachLoop(array, iteratee, reverse) {
    if (!array) {
        return;
    }
    var length = array.length,
        returnIteratee,
        i = reverse ? length - 1 : 0;

    if (reverse) {
        for (; i >= 0; i--) {
            returnIteratee = iteratee(array[i], i);
            if (returnIteratee === true) {
                returnIteratee = null;
                continue;
            }
            if (returnIteratee === false) {
                break;
            }
        }
    } else {
        for (; i < length; i++) {
            returnIteratee = iteratee(array[i], i);
            if (returnIteratee === true) {
                returnIteratee = null;
                continue;
            }
            if (returnIteratee === false) {
                break;
            }
        }
    }
}

exports.eachLoop = eachLoop;